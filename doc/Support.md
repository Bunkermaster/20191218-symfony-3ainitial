Symfony
===

https://gitlab.com/Bunkermaster/20191218-symfony-3ainitial

# Namespace

# Git

`git init` initialise un repo dans le repertoire courant
`git remote -v` liste les remotes du repo local courant

# Composer

Gestion de dependances.

`composer init`	

**Exemple:**
Projet, 1 dev en v1.0. Il utilise PdfMaker v1.0.
6 mois après un autre dev le rejoint. 
Projet, 2 dev en v1.2. Le 1er dev utilise PdfMaker v1.0 et le 2eme utilise PdfMaker v2.5.

`composer require`

```
composer show
symfony/polyfill-mbstring v1.13.1 Symfony polyfill for the Mbstring extension
symfony/var-dumper        v5.0.1  Symfony mechanism for exploring and dumping PHP variables
```

## Les commandes

```
: 1576663350:0;git push origin master
: 1576663410:0;php index.php
: 1576663419:0;composer require
: 1576663759:0;composer list
: 1576663762:0;composer show
: 1576663775:0;git status
: 1576663778:0;git add .
: 1576663798:0;git commit -m "composer.json and composer.lock"
: 1576663802:0;git push origin master
: 1576663833:0;php index.php
: 1576663849:0;l
: 1576663865:0;rm -rf vendor
: 1576663866:0;l
: 1576663940:0;php index.php
: 1576663974:0;composer install
: 1576663994:0;composer show
: 1576664068:0;composer require
: 1576664643:0;composer dump-autoload
: 1576664815:0;php index.php
: 1576664946:0;composer remove
: 1576664950:0;composer remove phpunit
: 1576664992:0;composer show
: 1576664998:0;composer remove phpunit/phpunit
: 1576665016:0;composer require --help
: 1576665093:0;composer require --dev
: 1576665133:0;pwd
: 1576665142:0;l .composer
: 1576665148:0;l .composer/cache
: 1576665176:0;df -h
: 1576665326:0;tail -n30 ~/.zsh_history
```

getcomposer.org

# SemVer 
majeur.mineur.fix
v1.2.5
si je change les signatures des fonctions...
v2.0
semver.org


# HTTP

https://tools.ietf.org/html/rfc7540

# MVC

![MVC][MVC.png]
