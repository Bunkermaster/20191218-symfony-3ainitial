<?php
namespace Interfaces;

interface ShootInterface
{
    public function shoot(): string;
}