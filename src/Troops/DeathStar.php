<?php
namespace Troops;

use Interfaces\ShootInterface;

class DeathStar implements ShootInterface
{
    public function shoot(): string
    {
        return "pewpewpewpewpew".PHP_EOL;
    }
}
