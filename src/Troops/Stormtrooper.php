<?php
namespace Troops;

use Interfaces\ShootInterface;

class Stormtrooper implements ShootInterface
{
    private $name = '';

    public function shoot(): string
    {
        return "Ouch";
    }


    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $name = ST_NAME_PREFIX.$name;
        $this->name = $name;
    }

    public function __construct(string $name)
    {
        $this->setName($name);
        echo "My name is ".$this->getName().PHP_EOL;
    }

    public function __destruct()
    {
        echo "Bam bam they shot ".$this->name." down".PHP_EOL;
    }

    public function __call($name, $arguments)
    {
        echo "You called $name, too bad".PHP_EOL;
    }
}
